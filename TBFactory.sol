pragma solidity ^0.6.0;

import "./Ownable.sol";
import "./SafeMath.sol";
import "./IERC20.sol";
import "./USDTWrapper.sol";

contract TBFactory is Ownable, USDTWrapper {
    //引用 SafeMath 在 uint 類型上，避免溢出。
    using SafeMath for uint256;
    using Address for address;

    uint256 public trumpVoteCount = 0; //投給川普的人數
    uint256 public bidenVoteCount = 0; //投給拜登的人數
    uint256 public trumpTotalAmount = 0; //投給川普的下注總額
    uint256 public bidenTotalAmount = 0; //投給拜登的下注總額
    uint256 public voteEndTime = 1604376000; //投票結束時間：11/3
    uint256 private taxTotalAmount = 0; //手續費總額
    uint8 private trumpTax = 1; //川普的手續費
    uint8 private bidenTax = 3; //拜登的手續費

    // 構造器：初始化
    constructor() public {
        usdt = IERC20(0xdac17f958d2ee523a2206206994597c13d831ec7);
    }

    // 結構體：下注憑證
    struct VoteProof {
        // 1 means Trump; 2 means Biden
        uint8 voteFor; //下注哪位候選人，1為川普; 2為拜登
        uint256 amount; //下注金額
    }

    // 將上述的結構體實例化成一組陣列
    VoteProof[] public voteProofs;

    // 將憑證映射到錢包地址
    mapping(uint256 => address) public proofToOwner;
    // 將錢包地址映射到憑證數量
    mapping(address => uint256) ownerProofsCount;

    // 事件：建立下注憑證
    event CreateProof(string voteFor, uint256 amount);

    // 修改器：確認是否還能投票
    modifier voteTime {
        require(now <= voteEndTime); //當前時間必須小於投票結束時間
        _;
    }

    // 方法：建立下注憑證的內部方法
    function _createProof(uint8 _voteFor, uint256 _amount) internal {
        voteProofs.push(VoteProof(_voteFor, _amount)); //建立一個憑證並推送到 voteProofs 陣列中
        uint256 id = voteProofs.length - 1; //為憑證建立一個 ID，用於將憑證編號，方便之後識別
        proofToOwner[id] = msg.sender; //將編號關聯到建立憑證的地址上
        ownerProofsCount[msg.sender] = ownerProofsCount[msg.sender].add(1); //將錢包地址的憑證數量加一，使用 SafeMath 方法避免溢出

        // 假如下注的候選人為川普
        if (_voteFor == 1) {
            trumpVoteCount = trumpVoteCount.add(1); //將川普的得票數加一，使用 SafeMath 方法避免溢出
            trumpTotalAmount = trumpTotalAmount.add(_amount); //將川普的下注總額加上此憑證的下注金額，使用 SafeMath 方法避免溢出
            emit CreateProof("VoteForTrump", _amount); //調用事件 CreateProof：建立下注憑證
        }

        // 假如下注的候選人為拜登
        if (_voteFor == 2) {
            bidenVoteCount = bidenVoteCount.add(1);
            bidenTotalAmount = bidenTotalAmount.add(_amount);
            emit CreateProof("VoteForBiden", _amount);
        }
    }

    // 方法：下注川普
    function voteForTrump(uint256 amount) external payable voteTime {
        send(amount);
        uint256 betAmount = amount - (amount.mul(trumpTax).div(100));
        _createProof(1, betAmount);
        taxTotalAmount = taxTotalAmount.add(amount.sub(betAmount)); //將手續費增加到手續費總額
    }

    // 方法：下注拜登
    function voteForBiden(uint256 amount) external payable voteTime {
        send(amount);
        uint256 betAmount = amount - (amount.mul(bidenTax).div(100));
        _createProof(2, betAmount);
        taxTotalAmount = taxTotalAmount.add(amount.sub(betAmount));
    }

    // 方法：修改手續費，僅限管理員從外部調用
    function setTrumpTax(uint8 _tax) external onlyOwner {
        trumpTax = _tax;
    }

    function setBidenTax(uint8 _tax) external onlyOwner {
        bidenTax = _tax;
    }

    function send(uint256 amount) public override {
        require(amount > 0, "Cannot send 0");
        super.send(amount);
    }
}
