pragma solidity ^0.6.0;

import "./TBFactory.sol";

contract TBHelper is TBFactory {
    // 1 for Trump; 2 for Biden; 0 for pending
    uint8 winner = 0; //獲勝的候選人，1為川普; 2為拜登; 0為尚未接收到結果

    // 事件：發送獲勝候選人編號
    event WeHaveWinner(string str);
    // 事件：獲得獎金
    event GetPrize(uint256 amount);

    // 修改器：贏家才能調用
    modifier onlyWinner(uint256 _proofId) {
        require(voteProofs[_proofId].voteFor == winner); //下注憑證的候選人編號必須為勝選的候選人編號，否則交易失敗
        _;
    }

    // 修改器：擁有者才能調用
    modifier onlyOwnerOf(uint256 _proofId) {
        require(proofToOwner[_proofId] == msg.sender); //下注憑證的持有人必須等於交易發送者，否則交易失敗
        _;
    }

    // 方法：設定贏家，僅限管理員從外部調用
    function setWinner(uint8 _voteFor) external onlyOwner {
        // 1 for Trump; 2 for Biden
        winner = _voteFor;

        // 假如贏家為川普
        if (winner == 1) {
            emit WeHaveWinner("The winner is Trump!"); //調用事件 WeHaveWinner：發送獲勝候選人編號
        }

        // 假如贏家為拜登
        if (winner == 2) {
            emit WeHaveWinner("The winner is Biden!");
        }
    }

    // 方法：刪除憑證的內部方法
    function _deleteProof(uint256 _proofId) internal {
        delete voteProofs[_proofId];
        proofToOwner[_proofId] = 0x0000000000000000000000000000000000000000;
        ownerProofsCount[msg.sender].sub(1);
    }

    // 方法：提款的內部方法
    function _quit(uint256 _loserPool) internal {
        uint256 _amount;
        for (uint256 i = 0; i < voteProofs.length; i++) {
            if (
                proofToOwner[i] == msg.sender && voteProofs[i].voteFor == winner
            ) {
                _amount = _amount.add(voteProofs[i].amount);
                _deleteProof(i);
            }
        }
        uint256 prize = _amount + _amount.mul(_loserPool);
        withdraw(prize);
        emit GetPrize(_amount);
    }

    // 方法：提款
    function quit() external payable {
        if (winner == 1) {
            uint256 loserPool = bidenTotalAmount.div(trumpTotalAmount);
            _quit(loserPool);
        }

        if (winner == 2) {
            uint256 loserPool = trumpTotalAmount.div(bidenTotalAmount);
            _quit(loserPool);
        }
    }

    // 方法：提出手續費，僅限管理員從外部調用
    function withdrawTax() external payable onlyOwner {
        uint256 _tax = taxTotalAmount;
        withdraw(_tax);
        _withdrawEth();
        taxTotalAmount = 0;
    }

    // 方法：獲得該錢包地址所有的下注憑證編號
    function getProofsByOwner(address _owner)
        external
        view
        returns (uint256[] memory)
    {
        uint256[] memory result = new uint256[](ownerProofsCount[_owner]);
        uint256 counter = 0;
        for (uint256 i = 0; i < voteProofs.length; i++) {
            if (proofToOwner[i] == _owner) {
                result[counter] = i;
                counter++;
            }
        }
        return result;
    }

    // 方法：取得合約內的金額，僅限管理員從外部調用
    function getBalance() external view onlyOwner returns (uint256) {
        return address(this).balance;
    }

    // 方法：提款
    function withdraw(uint256 amount) internal override {
        require(amount > 0, "Cannot withdraw 0");
        super.withdraw(amount);
    }

    // 方法：提取以太幣的內部方法
    function _withdrawEth() internal onlyOwner {
        msg.sender.transfer(address(this).balance);
    }

    // 方法：更改管理員，僅限管理員從外部調用
    function changeOwner(address _newOwner) external onlyOwner {
        transferOwnership(_newOwner);
    }

    // 方法：當超過 12/3 後，管理員可以提出還沒被領出的貨幣
    function quitAll(uint256 amount) external onlyOwner {
        require(now >= 1606968000);
        withdraw(amount);
    }


}
