// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.6.0;

import "./SafeMath.sol";
import "./SafeERC20.sol";

contract USDTWrapper {
    using SafeMath for uint256;
    using SafeERC20 for IERC20;

    IERC20 public usdt;

    uint256 private _totalSupply;
    mapping(address => uint256) private _balances;

    function totalSupply() public view returns (uint256) {
        return _totalSupply;
    }

    function balanceOf(address account) public view returns (uint256) {
        return _balances[account];
    }

    function send(uint256 amount) public virtual {
        _totalSupply = _totalSupply.add(amount);
        _balances[msg.sender] = _balances[msg.sender].add(amount);
        usdt.safeTransferFrom(msg.sender, address(this), amount);
    }

    function withdraw(uint256 amount) internal virtual {
        _totalSupply = _totalSupply.sub(amount);
        _balances[msg.sender] = _balances[msg.sender].sub(amount);
        usdt.safeTransfer(msg.sender, amount);
    }
}
